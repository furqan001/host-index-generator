const admin = require('firebase-admin');

let ref;

const getFirebaseRef = ({ configs }) => {
  if (!configs.project_id) {
    process.stdout.write('\n> Configs are corrupt\n');
    throw Error('process.env is corrupt');
  }
  if (ref) {
    return ref;
  }

  admin.initializeApp({
    credential: admin.credential.cert(configs),
    databaseURL: `https://${configs.project_id}.firebaseio.com`,
  });
  ref = admin.database().ref();
  return ref;
};

module.exports = {
  getFirebaseRef,
};
