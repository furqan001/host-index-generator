type Params = {
  isDev?: boolean;
  buildPath: string;
  firebaseConfigs: any;
  indexFilePath: string;
  databasePrefix?: string;
  propertiesOutputPath?: string;
};

export function startGenerateHostIndexWorker(params: Params): void;
