const tasks = require("./tasks");
const firebase = require("./firebase");

const startGenerateHostIndexWorker = ({
  isDev,
  buildPath,
  indexFilePath,
  databasePrefix,
  firebaseConfigs,
  propertiesOutputPath,
}) => {
  const firebaseRef = firebase.getFirebaseRef({ configs: firebaseConfigs });
  firebaseRef.on("value", (snapshot) => {
    let properties = snapshot.val();
    properties = databasePrefix ? properties[databasePrefix] : properties;

    if (isDev && propertiesOutputPath) {
      tasks.generateHostProperties({
        properties,
        outputPath: propertiesOutputPath,
      });
    } else if (!isDev) {
      tasks.generateIndexFromProperties({
        buildPath,
        indexFilePath,
        properties,
      });
    }
  });
};

module.exports = { startGenerateHostIndexWorker };
