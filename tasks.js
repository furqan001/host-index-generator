const fs = require("fs");
const crypto = require("crypto");
const merge = require("lodash/merge");
const { Spinner } = require("cli-spinner");

const spinner = new Spinner("processing... %s");

/**
 * generateProperty
 * This function takes properties, outputPath & fileName as parameters and generates
 * file in outputPath given as parameter
 *
 * @param {
 *  properties,
 *  outputPath,
 *  fileName
 * }
 */

function generateProperty({ properties, outputPath, fileName }) {
  return new Promise((resolve, reject) => {
    writeInFile(
      `${outputPath}/${fileName}.js`,
      `/* eslint-disable */\nmodule.exports = ${JSON.stringify(
        properties,
        null,
        2
      )}`
    )
      .then(() => {
        process.stdout.write(`> Successfully created ${fileName}.js \n`);
        return resolve();
      })
      .catch((e) => {
        process.stdout.write(`> Error while creating ${fileName}.js \n`);
        return reject(e);
      });
  });
}

/**
 * generateHostProperties
 * This function takes properties & outputPath as parameter and generates
 * individual host properties files in outputPath given as parameter
 *
 * @param {
 *  properties,
 *  outputPath
 * }
 */

function generateHostProperties({ properties, outputPath }) {
  spinner.start();
  const promises = [];
  try {
    !fs.existsSync(outputPath) && fs.mkdirSync(outputPath, { recursive: true });

    Object.keys(properties.HOSTS).forEach((key) => {
      const finalProperties = getPreparedProperties({
        environment: properties.ENVIRONMENT,
        host: properties.HOSTS[key],
      });
      const promise = generateProperty({
        properties: finalProperties,
        outputPath,
        fileName: key,
      });
      promises.push(promise);
    });
    return Promise.all(promises).then(() => spinner.stop());
  } catch (e) {
    spinner.stop();
    // eslint-disable-next-line no-console
    console.warn(e);
    process.exit();
  }
}

/**
 * transformMessagesKeys
 * Purpose: Since firebase doesn't allow '.' in keys and react-intl
 * messages key has '.'. We have added '_' in firebase DB and this method
 * will transform '_' to '.'
 * @param {
 *  messages
 * }
 */
const transformMessagesKeys = ({ messages }) =>
  Object.keys(messages).reduce(
    (acc, key) => ({
      ...acc,
      ...{ [key.replace(/_/g, ".")]: messages[key] },
    }),
    {}
  );

/**
 * writeInFile
 * Writes in file the content passed to this function at filePath given to it
 * @param {
 *  filePath,
 *  content
 * }
 */
function writeInFile(filePath, content) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, content, (error) => {
      if (error) {
        reject(error);
      }
      resolve();
    });
  });
}

/**
 * prepareHTML
 * Generates properties file with hashed file name and add it in the host html (e.g: hbl-instalment-web-index.html)
 * in the script tag
 * @param {
 *  buildPath,
 *  hostKey,
 *  configs,
 *  htmlContent
 * }
 */

function prepareHTML({ buildPath, hostKey, configs, htmlContent }) {
  const hashValue = crypto
    .createHash("sha256")
    .update(hostKey + new Date().getTime())
    .digest("hex");
  const configsFile = `${buildPath}/${hashValue}.js`;
  return new Promise((resolve, reject) => {
    writeInFile(
      configsFile,
      `/* eslint-disable */ \n window.__pkbg__ = ${JSON.stringify(
        configs,
        null,
        2
      )}`
    )
      .then(() => {
        process.stdout.write(
          `> ${hashValue}.js has been created in build folder. \n`
        );
        const file = htmlContent
          .toString()
          .replace(
            "<script configs></script>",
            `<script type="text/javascript" src="/${hashValue}.js"></script>`
          );
        const outputFile = `${buildPath}/${hostKey}-index.html`;
        return writeInFile(outputFile, file);
      })
      .then(() => {
        process.stdout.write(
          `> ${hostKey}-index.html has been created in build folder. \n`
        );
        resolve();
      })
      .catch(reject);
  });
}

function getPreparedProperties({ environment, host }) {
  const finalProperties = merge(environment, host);

  if (finalProperties.messages) {
    // Iterating over all languages
    Object.keys(finalProperties.messages).forEach((language) => {
      finalProperties.messages[language] = transformMessagesKeys({
        messages: finalProperties.messages[language],
      });
    });
  }

  return finalProperties;
}

/**
 * Generates index html file for all hosts available in properties object
 * @param {
 *  indexFilePath: example `${buildPath}/index-maker.html`
 * } param0
 */
function generateIndex({ indexFilePath, buildPath, properties = {} }) {
  return new Promise((resolve, reject) => {
    fs.readFile(indexFilePath, (err, htmlContent) => {
      if (err) {
        process.stdout.write(
          "> Could not find index-maker.html in build folder...\n"
        );
        throw err;
      }
      const promises = [];
      if (!Object.keys(properties).length) {
        reject();
      }

      Object.keys(properties.HOSTS).forEach((key) => {
        const preparedProps = getPreparedProperties({
          environment: properties.ENVIRONMENT,
          host: properties.HOSTS[key],
        });
        const promise = prepareHTML({
          buildPath,
          hostKey: key,
          configs: preparedProps,
          htmlContent,
        });
        promises.push(promise);
      });

      Promise.all(promises).then(resolve).catch(reject);
    });
  });
}

function generateIndexFromProperties({
  indexFilePath,
  buildPath,
  properties = {},
}) {
  spinner.start();
  generateIndex({
    indexFilePath,
    buildPath,
    properties,
  })
    .then(() => {
      spinner.stop();
    })
    .catch((e) => {
      spinner.stop();
      // eslint-disable-next-line no-console
      console.warn(e);
      process.exit();
    });
}

module.exports = {
  generateHostProperties,
  generateIndexFromProperties,
};
